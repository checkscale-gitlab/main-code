variable "name" {
  description = "Name of the swarm.  Note this has to be globally unique."
}

variable "managers" {
  description = "Number of managers in the swarm.  This should be an odd number otherwise there may be issues with raft consensus."
  default     = 1
}

variable "workers" {
  description = "Number of workers in the swarm."
  default     = 0
}

variable "instance_type" {
  description = "EC2 instance type."
  default     = "t3.micro"
}